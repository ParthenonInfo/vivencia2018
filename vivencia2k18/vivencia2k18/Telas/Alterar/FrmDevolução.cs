﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Emprestimo;
using vivencia2k18.Programação.Validação;

namespace vivencia2k18.Telas.Alterar
{
    public partial class FrmDevolução : Form
    {
        public FrmDevolução()
        {
            InitializeComponent();
        }
        Validação v = new Validação();


        EmprestimoView dto;
         public void Loadscreen(EmprestimoView dto)
        {
            this.dto = dto;
           lblaluno.Text = this.dto.nm_aluno;
           lbllivro.Text = this.dto.nm_livro;
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmDevolução_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        
            if (dtprenovaçao.Visible == false && dtprenovaçao.Enabled == false)
            {
                dtprenovaçao.Visible = true;
                dtprenovaçao.Enabled = true;
            }
            else
            {
                this.dto.dt_vencimento = dtprenovaçao.Value;
                EmprestimoViewDatabase database = new EmprestimoViewDatabase();
                database.Alteraçaodata(this.dto);
                MessageBox.Show("Prazo Renovado.");
                Close();

            }


    }

        private void btnAlterarLivro_Click(object sender, EventArgs e)
        {
            EmprestimoViewDatabase database = new EmprestimoViewDatabase();
            database.Update(this.dto);
            this.dto.ds_observacao = textBox1.Text;
            database.Alteraçao(this.dto);

            MessageBox.Show("Devolução registrada.");
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            Close();
        }

       

        private void dtprenovaçao_ValueChanged(object sender, EventArgs e)
        {

        }

 
    }
}
