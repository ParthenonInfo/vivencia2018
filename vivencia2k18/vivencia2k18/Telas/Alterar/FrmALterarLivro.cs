﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Categoria;
using vivencia2k18.Programação.Estoque;
using vivencia2k18.Programação.Livro;
using vivencia2k18.Programação.Validação;

namespace vivencia2k18.Telas.Alterar
{
    public partial class FrmALterarLivro : Form
    {
        public FrmALterarLivro()
        {
            InitializeComponent();
            carregar();
        }
        Validação v = new Validação();
        LivroViewDTO dto;
        


        void carregar()
        {
            CategoriaDatabase business = new CategoriaDatabase();
            List<LivroViewDTO> lista = business.Lista();

            cbmCategoria.ValueMember = nameof(LivroViewDTO.id_categoria);
            cbmCategoria.DisplayMember = nameof(LivroViewDTO.nm_categoria);
            cbmCategoria.DataSource = lista;
        }
   


        public void Loadscreen(LivroViewDTO dto)
        {
            this.dto = dto;
            cbmCategoria.Text = this.dto.nm_categoria;
            txtnomelivro.Text = this.dto.nm_livro;
            txtEdição.Text = this.dto.nm_autor;
            txteditora.Text = this.dto.nm_editora;
            txtcodigo.Text = this.dto.ds_codigolivro;
            dtppublicaçao.Value = this.dto.dt_publicacao;
            txtEdição.Text = this.dto.ds_volume;
            txtautor.Text = this.dto.nm_autor;
            txtquant.Text =this.dto.ds_quantidade.ToString();
            cbmCategoria.SelectedItem = this.dto.id_categoria.ToString("0.#");
      
          
        }



        private void FrmALterarLivro_Load(object sender, EventArgs e)
        {

        }

        private void btnCadastrarLivro_Click(object sender, EventArgs e)
        {
            if (chkUnica.Checked == true)
            {
                LivroViewDTO cat = cbmCategoria.SelectedItem as LivroViewDTO;
                int id_categoria = cat.id_categoria;
                dto.nm_livro = txtnomelivro.Text;
                dto.nm_editora = txteditora.Text;
                dto.nm_autor = txtautor.Text;
                dto.ds_codigolivro = txtcodigo.Text;
                dto.dt_publicacao = dtppublicaçao.Value.Date;
                dto.ds_volume = "1";
                dto.id_categoria = Convert.ToInt32(id_categoria);
                dto.ds_quantidade =Convert.ToInt32(txtquant.Text);
                LivroViewBusiness business = new LivroViewBusiness();
                LivroViewDatabase busines = new LivroViewDatabase();
                business.Altera(dto);
                busines.Update(dto);
                MessageBox.Show("Livro Alterado.");
                Close();

            }
            else
            {
                LivroViewDTO cat = cbmCategoria.SelectedItem as LivroViewDTO;
                int id_categoria = cat.id_categoria;
                dto.nm_livro = txtnomelivro.Text;
                dto.nm_editora = txteditora.Text;
                dto.nm_autor = txtautor.Text;
                dto.ds_codigolivro = txtcodigo.Text;
                dto.dt_publicacao = dtppublicaçao.Value.Date;
                dto.ds_volume = txtEdição.Text;
                dto.id_categoria = Convert.ToInt32(id_categoria);
                dto.ds_quantidade = Convert.ToInt32(txtquant.Text);
                LivroViewBusiness business = new LivroViewBusiness();
                LivroViewDatabase busines = new LivroViewDatabase();
                business.Altera(dto);
                busines.Update(dto);
                MessageBox.Show("Livro Alterado.");
                Close();
            }
           
        }

      

        private void chkUnica_CheckedChanged(object sender, EventArgs e)
        {
                    if (chkUnica.Checked == true)
            {
                txtEdição.Enabled = false;
                txtEdição.Visible = false;
            }
            else if (chkUnica.Checked == false)
            {
                txtEdição.Enabled = true;
                txtEdição.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
       
        }

        private void label9_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtnomelivro_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtEdição_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
