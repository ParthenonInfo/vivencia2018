﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Usuario;

namespace vivencia2k18.Telas
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {


                UsuarioBusiness business = new UsuarioBusiness();
                UsuarioDTO usuario = business.Logar(txtUsuario.Text, txtSenha.Text);

                if (usuario != null)
                {
                    UserSession.UsuarioLogado = usuario;

                    FrmMenu menuz = new FrmMenu();
                    this.Hide();
                    menuz.ShowDialog();


                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Livraria Frei", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Livraria Frei",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
