﻿using Nsf._2018.Modulo2.DB.Filosofia.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Aluno;
using vivencia2k18.Programação.Emprestimo;
using vivencia2k18.Telas.Alterar;
using vivencia2k18.Telas.Cadastro;
using vivencia2k18.Telas.Consulta;

namespace vivencia2k18.Telas
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        public void AbrirForminPanel(object Form)
        {
            if (this.PanelCorpo.Controls.Count > 0)
                this.PanelCorpo.Controls.RemoveAt(0);
            Form fh = Form as Form;
            fh.TopLevel = false;
            fh.FormBorderStyle = FormBorderStyle.None;
            fh.Dock = DockStyle.Fill;
            this.PanelCorpo.Controls.Add(fh);
            this.PanelCorpo.Tag = fh;
            fh.Show();
        }



   

  
        private void alunoToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        private void PanelMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void livroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmCadastroLivro());
        }

        private void usuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void alunoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmConsultarAluno());
        }

        private void livroToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmConsultaLivro());
        }

        private void empréstimoToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void empréstimoToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmEmprestimo());
        }

        private void devoluçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmDevolução());
        }

        private void consultaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void empréstimoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void estoqueToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void atrasosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmConsultarAtrasados());
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Email email = new Email();
            EmprestimoBusiness emprestimo = new EmprestimoBusiness();
            List<EmprestimoView> view = emprestimo.ConsultarView(string.Empty);
            List<AlunoDTO> aluno = new List<AlunoDTO>();
            foreach (EmprestimoView item in view)
            {
                AlunoDTO dto = new AlunoDTO { nm_aluno = item.nm_aluno };
                aluno.Add(dto);
            }

            
            EmprestimoBusiness emprestimos = new EmprestimoBusiness();
            List<EmprestimoView> views = emprestimo.ConsultarView(string.Empty);
            List<AlunoDTO> alunos = new List<AlunoDTO>();
            foreach (EmprestimoView item in view)
            {
                AlunoDTO dto = new AlunoDTO { nm_aluno = item.nm_aluno };
                aluno.Add(dto);
            }
            email.Enviarultimoemail(view, aluno);
        }

        private void empréstimosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmConsultaEmprestimo());
        }

        private void atrasosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmConsultarAtrasados());
        }

        private void devoluçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirForminPanel(new FrmConsultaDevoluções());
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
    }
}

