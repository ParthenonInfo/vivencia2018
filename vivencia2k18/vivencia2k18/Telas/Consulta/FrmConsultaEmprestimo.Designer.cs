﻿namespace vivencia2k18.Telas.Consulta
{
    partial class FrmConsultaEmprestimo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConsultaEmprestimo));
            this.label2 = new System.Windows.Forms.Label();
            this.btnConsultarDevolucao = new System.Windows.Forms.Button();
            this.txtConsultaDevolucao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvlivro = new System.Windows.Forms.DataGridView();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Devolução = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlivro)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(186, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 39);
            this.label2.TabIndex = 70;
            this.label2.Text = "Consultar Empréstimo";
            // 
            // btnConsultarDevolucao
            // 
            this.btnConsultarDevolucao.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultarDevolucao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultarDevolucao.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarDevolucao.ForeColor = System.Drawing.Color.White;
            this.btnConsultarDevolucao.Location = new System.Drawing.Point(559, 51);
            this.btnConsultarDevolucao.Name = "btnConsultarDevolucao";
            this.btnConsultarDevolucao.Size = new System.Drawing.Size(142, 33);
            this.btnConsultarDevolucao.TabIndex = 67;
            this.btnConsultarDevolucao.Text = "Consultar";
            this.btnConsultarDevolucao.UseVisualStyleBackColor = false;
            this.btnConsultarDevolucao.Click += new System.EventHandler(this.btnConsultarEmprestimo_Click);
            // 
            // txtConsultaDevolucao
            // 
            this.txtConsultaDevolucao.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConsultaDevolucao.Location = new System.Drawing.Point(125, 51);
            this.txtConsultaDevolucao.Name = "txtConsultaDevolucao";
            this.txtConsultaDevolucao.Size = new System.Drawing.Size(428, 31);
            this.txtConsultaDevolucao.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(25, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 30);
            this.label1.TabIndex = 69;
            this.label1.Text = "Data";
            // 
            // dgvlivro
            // 
            this.dgvlivro.AllowUserToAddRows = false;
            this.dgvlivro.AllowUserToDeleteRows = false;
            this.dgvlivro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvlivro.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvlivro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlivro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Devolução});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvlivro.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvlivro.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvlivro.Location = new System.Drawing.Point(0, 100);
            this.dgvlivro.Name = "dgvlivro";
            this.dgvlivro.ReadOnly = true;
            this.dgvlivro.RowHeadersVisible = false;
            this.dgvlivro.Size = new System.Drawing.Size(725, 239);
            this.dgvlivro.TabIndex = 68;
            this.dgvlivro.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlivro_CellContentClick);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::vivencia2k18.Properties.Resources.Lapis;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 25;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "nm_aluno";
            this.Column2.HeaderText = "Aluno";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "nm_livro";
            this.Column3.HeaderText = "Livro";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.DataPropertyName = "dt_emprestimo";
            this.Column4.HeaderText = "Emissão";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.DataPropertyName = "dt_vencimento";
            this.Column5.HeaderText = "Devolução";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Devolução
            // 
            this.Devolução.HeaderText = "";
            this.Devolução.Image = global::vivencia2k18.Properties.Resources.Lapis;
            this.Devolução.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Devolução.Name = "Devolução";
            this.Devolução.ReadOnly = true;
            this.Devolução.Width = 25;
            // 
            // FrmConsultaEmprestimo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(725, 339);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnConsultarDevolucao);
            this.Controls.Add(this.txtConsultaDevolucao);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvlivro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmConsultaEmprestimo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmConsultaEmprestimo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlivro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnConsultarDevolucao;
        private System.Windows.Forms.TextBox txtConsultaDevolucao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvlivro;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewImageColumn Devolução;
    }
}