﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Emprestimo;
using vivencia2k18.Programação.Validação;

namespace vivencia2k18.Telas.Consulta
{
    public partial class FrmConsultarAtrasados : Form
    {
        public FrmConsultarAtrasados()
        {
            InitializeComponent();
            dgvlivro.AutoGenerateColumns = false;
        }
        Validação v = new Validação();

        private void btnConsultarAtrasados_Click(object sender, EventArgs e)
        {

            DateTime hoje = DateTime.Now;
            EmprestimoViewDatabase business = new EmprestimoViewDatabase();
            List<AtrasoView> lista = business.ConsultarAtrasos();

            dgvlivro.AutoGenerateColumns = false;
            dgvlivro.DataSource = lista;
        }
    }
}
