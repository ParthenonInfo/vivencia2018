﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Livro;
using vivencia2k18.Programação.Validação;
using vivencia2k18.Telas.Alterar;

namespace vivencia2k18.Telas.Consulta
{
    public partial class FrmConsultaLivro : Form
    {
        public FrmConsultaLivro()
        {
            InitializeComponent();
            dgvlivro.AutoGenerateColumns = false;
        }
        Validação v = new Validação();

        private void btnConsultarAluno_Click(object sender, EventArgs e)
        {
            LivroViewBusiness business = new LivroViewBusiness();
            List<LivroViewDTO> lista = business.Consultar(txtConsultaLivro.Text.Trim());

            dgvlivro.AutoGenerateColumns = false;
            dgvlivro.DataSource = lista;
        }

        private void FrmConsultaLivro_Load(object sender, EventArgs e)
        {

        }

        private void dgvlivro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                LivroViewDTO comp = dgvlivro.Rows[e.RowIndex].DataBoundItem as LivroViewDTO;
                FrmALterarLivro Load = new FrmALterarLivro();
                Load.Loadscreen(comp);
                Load.Show();

            }

            if (e.ColumnIndex == 9)
            {
                DialogResult r = MessageBox.Show("Deseja excluir o livro?", "Biblioteca", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (r == DialogResult.Yes)
                {
                    LivroViewDTO forn = dgvlivro.Rows[e.RowIndex].DataBoundItem as LivroViewDTO;

                    LivroViewDatabase database = new LivroViewDatabase();
                    database.Remover(forn.id_livro);

                    MessageBox.Show("Registro Removido com sucesso", "Biblioteca", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    LivroViewBusiness business = new LivroViewBusiness();
                    List<LivroViewDTO> lista = business.Listar();
                    dgvlivro.DataSource = null;
                    dgvlivro.DataSource = lista;
                }
            }

                }
    }
}
