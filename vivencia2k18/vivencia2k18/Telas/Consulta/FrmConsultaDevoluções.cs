﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Emprestimo;

namespace vivencia2k18.Telas.Consulta
{
    public partial class FrmConsultaDevoluções : Form
    {
        public FrmConsultaDevoluções()
        {
            InitializeComponent();
        }

        private void btnConsultarDevolução_Click(object sender, EventArgs e)
        {
            Emprestimo_devolucao_view_Database database = new Emprestimo_devolucao_view_Database();
          List<Emprestimo_devolucao_view> dto= database.Listar();


            dgvdevolucao.DataSource = null;
            dgvdevolucao.DataSource = dto;


        }

        private void dgvdevolucao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
