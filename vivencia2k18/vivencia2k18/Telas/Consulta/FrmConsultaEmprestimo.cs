﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Emprestimo;
using vivencia2k18.Programação.Validação;
using vivencia2k18.Telas.Alterar;

namespace vivencia2k18.Telas.Consulta
{
    public partial class FrmConsultaEmprestimo : Form
    {
        public FrmConsultaEmprestimo()
        {
            InitializeComponent();
            dgvlivro.AutoGenerateColumns = false;
        }
        Validação v = new Validação();

        private void btnConsultarEmprestimo_Click(object sender, EventArgs e)
        {
            EmprestimoViewDatabase business = new EmprestimoViewDatabase();
            List<EmprestimoView> lista = business.Consultar(txtConsultaDevolucao.Text.Trim());

            dgvlivro.AutoGenerateColumns = false;
            dgvlivro.DataSource = lista;
        }

        private void dgvlivro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
               EmprestimoView comp = dgvlivro.Rows[e.RowIndex].DataBoundItem as EmprestimoView;
                FrmDevolução Load = new FrmDevolução();
                Load.Loadscreen(comp);
                Load.ShowDialog();
                btnConsultarEmprestimo_Click(null, null);

            }
        }

        private void FrmConsultaEmprestimo_Load(object sender, EventArgs e)
        {

        }
    }
}
