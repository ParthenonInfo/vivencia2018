﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vivencia2k18.Telas.Consulta
{
    public partial class FrmEstoquedeLivros : Form
    {
        public FrmEstoquedeLivros()
        {
            InitializeComponent();

            List<tb_Curso> cursos = db.tb_Curso.ToList();
            dataGridView2.AutoGenerateColumns = false;
            dataGridView2.DataSource = cursos;
        }

        private void btnConsultarEstoque_Click(object sender, EventArgs e)
        {
            List<tb_aluno> alunos = db.tb_aluno
                                      .Where(x => x.nm_aluno.StartsWith(txtBusca.Text))
                                      .ToList();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = alunos;
        }
    }
}
