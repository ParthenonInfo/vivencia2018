﻿using nsf._2018.diferenciais.Dispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Autor;
using vivencia2k18.Programação.Categoria;
using vivencia2k18.Programação.Editora;
using vivencia2k18.Programação.Estoque;
using vivencia2k18.Programação.Livro;
using vivencia2k18.Programação.Validação;

namespace vivencia2k18.Telas.Cadastro
{
    public partial class FrmCadastroLivro : Form
    {
        public FrmCadastroLivro()
        {
            InitializeComponent();
            carregar();
        }
        Validação v = new Validação();

        private void btnCadastrarLivro_Click(object sender, EventArgs e)
        {
            CategoriaDTO cat = cbmCategoria.SelectedItem as CategoriaDTO;
            LivroDTO dtoLivro = new LivroDTO();

            if (chkUnica.Checked == true)
            {
                txtEdicao.Enabled = false;
                txtEdicao.Visible = false;
                dtoLivro.nm_livro = txtnomelivro.Text;
            dtoLivro.nm_editora = txtEditora.Text;
            dtoLivro.nm_autor = txtAutor.Text;
            dtoLivro.nm_livro = txtnomelivro.Text;
            dtoLivro.id_categoria = cat;
            dtoLivro.ds_codigolivro = txtCodigo.Text;
            dtoLivro.dt_publicacao = dtpPublicacao.Value.Date;
            dtoLivro.ds_volume = "1";

                EstoqueDTO estoque = new EstoqueDTO();
            estoque.ds_quantidade = Convert.ToInt32(txtquan.Text);

            LivroBusiness business = new LivroBusiness();
            business.Salvar(dtoLivro, estoque);

            MessageBox.Show("Livro cadastrado.");

        }

            else
            {

                dtoLivro.nm_livro = txtnomelivro.Text;
                dtoLivro.nm_editora = txtEditora.Text;
                dtoLivro.nm_autor = txtAutor.Text;
                dtoLivro.nm_livro = txtnomelivro.Text;
                dtoLivro.id_categoria = cat;
                dtoLivro.ds_codigolivro = txtCodigo.Text;
                dtoLivro.dt_publicacao = dtpPublicacao.Value.Date;
                dtoLivro.ds_volume = txtEdicao.Text;

                EstoqueDTO estoque = new EstoqueDTO();
                estoque.ds_quantidade = Convert.ToInt32(txtquan.Text);

                LivroBusiness business = new LivroBusiness();
                business.Salvar(dtoLivro, estoque);

                MessageBox.Show("Livro cadastrado.");
            }



        }

        void carregar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            List<CategoriaDTO> lista = business.Listar();

            cbmCategoria.ValueMember = nameof(CategoriaDTO.id_categoria);
            cbmCategoria.DisplayMember = nameof(CategoriaDTO.nm_categoria);
            cbmCategoria.DataSource = lista;
        }


        private void FrmCadastroLivro_Load(object sender, EventArgs e)
        {

        }

        private void cbmCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkUnica_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUnica.Checked == true)
            {
                txtEdicao.Enabled = false;
                txtEdicao.Visible = false;
            }
            else if (chkUnica.Checked == false)
            {
                txtEdicao.Enabled = true;
                txtEdicao.Visible = true;
            }

        }

        private void FrmCadastroLivro_Load_1(object sender, EventArgs e)
        {

        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text != string.Empty)
            {
                MessageBox.Show("O código digitado será descartado e será gerado um novo código aleatório");
                txtCodigo.Text = string.Empty;
            }
            Random r = new Random();
            int i = r.Next(1000000,9999999);
            int b = r.Next(100000, 999999);
            string codigo = i.ToString() + b.ToString();
            txtCodigo.Text = codigo;

            SaveFileDialog janela = new SaveFileDialog();
            janela.Filter = "Imagens (*.png)|*.png";

            DialogResult result = janela.ShowDialog();

            if (result == DialogResult.OK)
            {
                BarCodeService barcode = new BarCodeService();
                Image imagem = barcode.SalvarCodigoBarras(txtCodigo.Text, janela.FileName);

                
            }
        }

        private void txtnomelivro_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void cbmCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtEdicao_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtEditora_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }
    }
}
