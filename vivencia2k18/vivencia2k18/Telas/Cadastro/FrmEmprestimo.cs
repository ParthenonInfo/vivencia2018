﻿using Nsf._2018.Modulo2.DB.Filosofia.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vivencia2k18.Programação.Aluno;
using vivencia2k18.Programação.Emprestimo;
using vivencia2k18.Programação.Livro;
using vivencia2k18.Programação.Validação;

namespace vivencia2k18.Telas
{
    public partial class FrmEmprestimo : Form
    {
        public FrmEmprestimo()
        {
            InitializeComponent();
            dgvbibli.AutoGenerateColumns = false;
            carregarcombo();
            carregar();

        }
        Validação v = new Validação();
        public LivroDTO dto { get; set; }
        public int Qtd { get; set; }

        public void carregarcombo()
        {
            LivroBusiness ok = new LivroBusiness();
            List<LivroDTO> com = ok.Listar();

            cbmLivro.DataSource = com;
            cbmLivro.DisplayMember = "nm_livro";
            cbmLivro.ValueMember = "id_livro";

        }

        public void carregar()
        {
            AlunoBusiness ok = new AlunoBusiness();
            List<AlunoDTO> com = ok.Listar();

            cbmAluno.DataSource = com;
            cbmAluno.DisplayMember = "nm_aluno";
            cbmAluno.ValueMember = "id_aluno";

        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                LivroDTO livro = cbmLivro.SelectedItem as LivroDTO;



                this.dto = livro;

                int qtd = 1;

                this.Qtd = qtd;

                if (dto != null)
                {
                    List<LivroDTO> prods = dgvbibli.DataSource as List<LivroDTO>;
                    if (prods == null)
                        prods = new List<LivroDTO>();
                    foreach (LivroDTO item in prods)
                    {
                        if (item.id_livro == dto.id_livro)
                        {
                            throw new ArgumentException("Este livro já está no carrinho, favor selecionar outro");
                        }
                    }

                    for (int i = 0; i < Qtd; i++)
                    {
                        prods.Add(dto);
                    }
                    dgvbibli.DataSource = null;
                    dgvbibli.DataSource = prods;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

    

    private void btnCadastrarLivro_Click(object sender, EventArgs e)
    {
            try
            {
                AlunoDTO forn = cbmAluno.SelectedItem as AlunoDTO;

                EmprestimoDTO teste = new EmprestimoDTO();
                teste.dt_emprestimo = dtp1.Value;
                teste.dt_vencimento = dtp2.Value;
                teste.id_aluno = forn;

                List<LivroDTO> itens = dgvbibli.DataSource as List<LivroDTO>;
                List<Emprestimo_Item_DTO> item = null; ;

                if (itens != null)
                {
                    foreach (LivroDTO ite in itens)
                    {
                        item = new List<Emprestimo_Item_DTO>();
                        Emprestimo_Item_DTO itemvenda = new Emprestimo_Item_DTO();
                        itemvenda.livro = ite;

                        item.Add(itemvenda);
                    }
                }
                EmprestimoBusiness bus = new EmprestimoBusiness();
                if (dgvbibli.DataSource == null)
                {
                    throw new ArgumentException("O carrinho está vazio.");
                }
                else
                {
                    bus.Salvar(teste, item);

                }


                foreach (DataGridViewRow row in dgvbibli.Rows)
                {
                    LivroDTO dto = row.DataBoundItem as LivroDTO;
                    int id = dto.id_livro;

                    EmprestimoDatabase database = new EmprestimoDatabase();

                    database.Updatemais(id);

                }
                MessageBox.Show("Empréstimo feito.");
                Email email = new Email();
                email.Assunto = "Empréstimo";
                AlunoDTO aluno = cbmAluno.SelectedItem as AlunoDTO;
                email.Mensagem = "Olá " + aluno.nm_aluno + " O seu empréstimo foi feito com sucesso você tem até o dia " + dtp2.Value.ToShortDateString() + " para devolver o livro.";
                email.Para = "drakmatop@gmail.com";
                email.Enviar();
                MessageBox.Show("E-mail enviado com sucesso");
                button1_Click_1(null, null);

            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
            
            
        }

      
        private void FrmEmprestimo_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dgvbibli_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtnomelivro_KeyPress(object sender, KeyPressEventArgs e)
        {
            

            
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigo.Text.Length == 13)
            {
                LivroBusiness business = new LivroBusiness();
                LivroDTO dTO = business.ConsultarPeloCodigo(txtCodigo.Text.Trim());
                if (dTO == null)
                {
                    MessageBox.Show("O livro não consta na base de dados");
                }
                else
                {
                    List<LivroDTO> prods = dgvbibli.DataSource as List<LivroDTO>;
                    if (prods == null)
                        prods = new List<LivroDTO>();


                    for (int i = 0; i < 1; i++)
                    {
                        prods.Add(dTO);
                    }
                    dgvbibli.DataSource = null;
                    dgvbibli.DataSource = prods;

                }
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            Email email = new Email();
            email.Assunto = "Empréstimo";
            AlunoDTO aluno = cbmAluno.SelectedItem as AlunoDTO;
            email.Mensagem = "Olá " + aluno.nm_aluno + " O seu empréstimo foi feito com sucesso você tem até o dia " + dtp2.Value.Date.ToString() + " para devolver o livro.";
            email.Para = "drakmatop@gmail.com";
            email.Enviar();
            MessageBox.Show("E-mail enviado com sucesso");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dgvbibli.DataSource = null;
        }

        private void cbmAluno_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }
    }
}


