﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.Programação.Autor;
using vivencia2k18.Programação.Categoria;
using vivencia2k18.Programação.Editora;

namespace vivencia2k18.Programação.Livro
{
  public  class LivroDTO
    {
        public int id_livro { get; set; }
        public CategoriaDTO id_categoria { get; set; }
        public string nm_livro { get; set; }
        public string nm_editora { get; set; }
        public string nm_autor { get; set; }
        public DateTime dt_publicacao { get; set; }
        public string ds_codigolivro { get; set; }
        public string ds_volume { get; set; }

    }
}
