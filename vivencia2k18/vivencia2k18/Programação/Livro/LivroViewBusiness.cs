﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.Programação.Estoque;

namespace vivencia2k18.Programação.Livro
{
    public  class LivroViewBusiness
    {
        public List<LivroViewDTO> Listar()
        {
            LivroViewDatabase db = new LivroViewDatabase();
            return db.Listar();
        }

        public List<LivroViewDTO> Consultar(string livro)
        {
            LivroViewDatabase db = new LivroViewDatabase();
            return db.Consultar(livro);
        }
        public void Altera (LivroViewDTO dto)
        {
            LivroViewDatabase business = new LivroViewDatabase();
            business.Alterar(dto); 

        }
        public void delete(int id)
        {
            LivroViewDatabase business = new LivroViewDatabase();
            business.Remover(id);

        }

    }
}
