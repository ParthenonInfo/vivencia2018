﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.Programação.Estoque;

namespace vivencia2k18.Programação.Livro
{
    public class LivroBusiness
    {
        public int Salvar(LivroDTO livro)
        {
            LivroDatabase db = new LivroDatabase();
            return db.Salvar(livro);
        }
        public List<LivroDTO> Consultar(string livro)
        {
            LivroDatabase db = new LivroDatabase();
            return db.Consultar(livro);
        }
        public void Alterar(LivroDTO dto)
        {
            LivroDatabase db = new LivroDatabase();
            db.Alterar(dto);
        }


        public void Remover(int id)
        {
            LivroDatabase db = new LivroDatabase();
            db.Remover(id);
        }
        public List<LivroDTO> Listar()
        {
            LivroDatabase db = new LivroDatabase();
            return db.Listar();
        }

        public void Salvar(LivroDTO livro, EstoqueDTO estoque)
        {
            LivroDatabase db = new LivroDatabase();
            int pk = db.Salvar(livro);


            estoque.id_livro = new LivroDTO();
            estoque.id_livro.id_livro = pk;

            EstoqueBusiness business = new EstoqueBusiness();
            business.Salvar(estoque);

        }

        public LivroDTO ConsultarPeloCodigo(string codigo)
        {
            LivroDatabase db = new LivroDatabase();
            return db.ConsultarPeloCodigo(codigo);
        }

    }
}
