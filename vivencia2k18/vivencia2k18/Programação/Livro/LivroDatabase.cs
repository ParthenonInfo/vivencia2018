﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;
using vivencia2k18.Programação.Autor;
using vivencia2k18.Programação.Categoria;
using vivencia2k18.Programação.Editora;

namespace vivencia2k18.Programação.Livro
{
   public class LivroDatabase
    {
        public int Salvar(LivroDTO dto)
        {
            string script = @" INSERT INTO tb_livro 
            (id_categoria, nm_livro, nm_editora, nm_autor, ds_volume, dt_publicacao, ds_codigolivro) 
            VALUES (@id_categoria, @nm_livro, @nm_editora, @nm_autor, @ds_volume, @dt_publicacao, @ds_codigolivro)";
           

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_categoria", dto.id_categoria.id_categoria));
            parms.Add(new MySqlParameter("nm_livro", dto.nm_livro));
            parms.Add(new MySqlParameter("nm_editora", dto.nm_editora));
            parms.Add(new MySqlParameter("nm_autor", dto.nm_autor));
            parms.Add(new MySqlParameter("dt_publicacao", dto.dt_publicacao));
            parms.Add(new MySqlParameter("ds_codigolivro", dto.ds_codigolivro));
            parms.Add(new MySqlParameter("ds_volume", dto.ds_volume));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(LivroDTO dto)
        {
            string script = @"UPDATE tb_livro set nm_autor = @nm_autor, id_categoria = @id_categoria, nm_editora = @nm_editora, nm_livro = @nm_livro, dt_publicacao = @dt_publicacao, ds_codigolivro = @ds_codigolivro, ds_volume= @ds_volume
                                    
                                     WHERE id_livro = @id_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", dto.id_livro));
            parms.Add(new MySqlParameter("nm_livro", dto.nm_livro));
            parms.Add(new MySqlParameter("nm_editora", dto.nm_editora));
            parms.Add(new MySqlParameter("nm_autor", dto.nm_autor));
            parms.Add(new MySqlParameter("dt_publicacao", dto.dt_publicacao));
            parms.Add(new MySqlParameter("ds_codigolivro", dto.ds_codigolivro));
            parms.Add(new MySqlParameter("ds_volume", dto.ds_volume));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {

            string script = @"DELETE FROM tb_livro WHERE id_livro = @id_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<LivroDTO> Consultar(string livro)
        {
            string script = @"SELECT * FROM tb_livro WHERE nm_livro like @nm_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro", livro + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<LivroDTO> lista = new List<LivroDTO>();
            while (reader.Read())
            {
                LivroDTO dto = new LivroDTO();
                dto.id_livro = reader.GetInt32("id_livro");
                dto.id_categoria= new CategoriaDTO();
                dto.id_categoria.id_categoria= reader.GetInt32("id_categoria");
                dto.nm_autor = reader.GetString("nm_autor");
                dto.nm_editora = reader.GetString("nm_editora");
                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_publicacao = reader.GetDateTime("dt_publicacao");
                dto.ds_codigolivro = reader.GetString("ds_codigolivro");
                dto.ds_volume = reader.GetString("ds_volume");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public LivroDTO ConsultarPeloCodigo(string codigo)
        {
            string script = @"SELECT * FROM tb_livro WHERE ds_codigolivro = @ds_codigolivro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_codigolivro", codigo));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            LivroDTO dto = null;

            while (reader.Read())
            {
                dto = new LivroDTO();
                dto.id_livro = reader.GetInt32("id_livro");
                dto.id_categoria = new CategoriaDTO();
                dto.id_categoria.id_categoria = reader.GetInt32("id_categoria");
                dto.nm_autor = reader.GetString("nm_autor");
                dto.nm_editora = reader.GetString("nm_editora");
                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_publicacao = reader.GetDateTime("dt_publicacao");
                dto.ds_codigolivro = reader.GetString("ds_codigolivro");
                dto.ds_volume = reader.GetString("ds_volume");
            }
            reader.Close();

            return dto;
        }

        public List<LivroDTO> Listar()
        {
            string script = "select * from tb_livro";
     

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<LivroDTO> listar = new List<LivroDTO>();

            while (reader.Read())
            {
                LivroDTO dto = new LivroDTO();
                dto.id_livro = reader.GetInt32("id_livro");
                dto.id_categoria = new CategoriaDTO();
                dto.id_categoria.id_categoria = reader.GetInt32("id_categoria");
                dto.nm_autor = reader.GetString("nm_autor");
                dto.nm_editora = reader.GetString("nm_editora");
                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_publicacao = reader.GetDateTime("dt_publicacao");
                dto.ds_codigolivro = reader.GetString("ds_codigolivro");
                dto.ds_volume = reader.GetString("ds_volume");



                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }





    }
}