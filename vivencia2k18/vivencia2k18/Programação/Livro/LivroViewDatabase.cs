﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;
using vivencia2k18.Programação.Categoria;

namespace vivencia2k18.Programação.Livro
{
    public class LivroViewDatabase
    {
        public List<LivroViewDTO> Listar()
        {
            string script = "select * from estoquera";


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<LivroViewDTO> listar = new List<LivroViewDTO>();

            while (reader.Read())
            {
                LivroViewDTO dto = new LivroViewDTO();
                dto.id_livro = reader.GetInt32("id_livro");
                dto.nm_categoria = reader.GetString("nm_categoria");
                dto.nm_autor = reader.GetString("nm_autor");
                dto.nm_editora = reader.GetString("nm_editora");
                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_publicacao = reader.GetDateTime("dt_publicacao");
                dto.ds_codigolivro = reader.GetString("ds_codigolivro");
                dto.ds_volume = reader.GetString("ds_volume");
                dto.ds_quantidade = reader.GetInt32("ds_quantidade");
                dto.ds_quantidade_emprestada = reader.GetInt32("ds_quantidade_emprestada");


                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }

        public List<LivroViewDTO> Consultar(string livro)
        {
            string script = @"SELECT * FROM estoquera WHERE nm_livro like @nm_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro", livro + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<LivroViewDTO> lista = new List<LivroViewDTO>();
            while (reader.Read())
            {
                LivroViewDTO dto = new LivroViewDTO();
                dto.id_livro = reader.GetInt32("id_livro");
                dto.nm_categoria = reader.GetString("nm_categoria");
                dto.nm_autor = reader.GetString("nm_autor");
                dto.nm_editora = reader.GetString("nm_editora");
                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_publicacao = reader.GetDateTime("dt_publicacao");
                dto.ds_codigolivro = reader.GetString("ds_codigolivro");
                dto.ds_volume = reader.GetString("ds_volume");
                dto.ds_quantidade = reader.GetInt32("ds_quantidade");
                dto.ds_quantidade_emprestada = reader.GetInt32("ds_quantidade_emprestada");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Alterar(LivroViewDTO dto)
        {
            string script = @"UPDATE tb_livro set nm_autor = @nm_autor,id_categoria = @id_categoria, nm_editora = @nm_editora, nm_livro = @nm_livro, dt_publicacao = @dt_publicacao, ds_codigolivro = @ds_codigolivro, ds_volume= @ds_volume
                                    
                                     WHERE id_livro = @id_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", dto.id_livro));
            parms.Add(new MySqlParameter("nm_livro", dto.nm_livro));
            parms.Add(new MySqlParameter("id_categoria", dto.id_categoria));
            parms.Add(new MySqlParameter("nm_editora", dto.nm_editora));
            parms.Add(new MySqlParameter("nm_autor", dto.nm_autor));
            parms.Add(new MySqlParameter("dt_publicacao", dto.dt_publicacao));
            parms.Add(new MySqlParameter("ds_codigolivro", dto.ds_codigolivro));
            parms.Add(new MySqlParameter("ds_volume", dto.ds_volume));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_livro WHERE id_livro = @id_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Update(LivroViewDTO dto)
        {
            string script = @"UPDATE tb_estoque SET ds_quantidade = @ds_quantidade  WHERE id_livro = @id_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", dto.id_livro));
            parms.Add(new MySqlParameter("ds_quantidade", dto.ds_quantidade));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }


    }
}
