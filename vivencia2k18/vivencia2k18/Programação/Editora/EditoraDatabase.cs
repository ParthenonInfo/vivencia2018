﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Editora
{
   public class EditoraDatabase
    {
        public int Salvar(EditoraDTO dto)
        {
            string script = @" INSERT INTO tb_editora 
            (nm_editora) 
            VALUES (@nm_editora)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_editora", dto.nm_editora));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(EditoraDTO dto)
        {
            string script = @"UPDATE tb_editora 
                                 SET nm_editora = @nm_editora 

                                     WHERE id_editora = @id_editora";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_editora", dto.nm_editora));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {

            string script = @"DELETE FROM tb_editora WHERE id_editora = @id_editora";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_editora", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EditoraDTO> Consultar(string editor)
        {
            string script = @"SELECT * FROM tb_editora WHERE nm_editora like @nm_editora";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_editora", editor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EditoraDTO> lista = new List<EditoraDTO>();
            while (reader.Read())
            {
                EditoraDTO dto = new EditoraDTO();
                dto.nm_editora = reader.GetString("nm_editora");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<EditoraDTO> Listar()
        {
            string script = "select * from tb_editora";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EditoraDTO> listar = new List<EditoraDTO>();

            while (reader.Read())
            {
                EditoraDTO dto = new EditoraDTO();
                dto.nm_editora = reader.GetString("nm_editora");

                listar.Add(dto);

            }
            reader.Close();
            return listar;

        }
    }
}
