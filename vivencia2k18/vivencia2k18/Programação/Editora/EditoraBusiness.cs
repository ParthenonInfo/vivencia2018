﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Editora
{
  public  class EditoraBusiness
    {
        public int Salvar(EditoraDTO dto)
        {
            EditoraDatabase db = new EditoraDatabase();
            return db.Salvar(dto);
        }
        public List<EditoraDTO> Consultar(string editor)
        {
            EditoraDatabase db = new EditoraDatabase();
            return db.Consultar(editor);
        }
        public void Alterar(EditoraDTO dto)
        {
            EditoraDatabase db = new EditoraDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            EditoraDatabase db = new EditoraDatabase();
            db.Remover(id);
        }
        public List<EditoraDTO> Listar()
        {
            EditoraDatabase db = new EditoraDatabase();
            return db.Listar();
        }
    }
}
