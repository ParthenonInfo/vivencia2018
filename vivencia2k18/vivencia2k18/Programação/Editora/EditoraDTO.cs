﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Editora
{
  public  class EditoraDTO
    {
        public int id_editora { get; set; }
        public string nm_editora { get; set; }
    }
}
