﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Estoque
{
    public class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script =
                @"INSERT INTO `db_biblioteca`.`tb_estoque` (id_livro, ds_quantidade,ds_quantidade_emprestada) VALUES (@id_livro, @ds_quantidade,0)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", dto.id_livro.id_livro));
            parms.Add(new MySqlParameter("ds_quantidade", dto.ds_quantidade));
            


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }


        public void Update(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_livro 
                                 SET ds_quantidade = @ds_quantidade,
                                    
                                     WHERE id_livro = @id_livro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_quantidade", dto.id_livro));
            parms.Add(new MySqlParameter("ds_quantidade", dto.ds_quantidade));
 

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }



    }
}
