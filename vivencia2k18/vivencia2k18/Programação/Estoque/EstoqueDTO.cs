﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.Programação.Livro;

namespace vivencia2k18.Programação.Estoque
{
    public class EstoqueDTO
    {
        public int id_estoque { get; set; }
        public LivroDTO id_livro { get; set; }
        public int ds_quantidade { get; set; }
        public int ds_quantidade_emprestada { get; set; }
    }
}
