﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;
using vivencia2k18.Programação.Livro;

namespace vivencia2k18.Programação.Categoria
{
 public   class CategoriaDatabase
    {
        public int Salvar(CategoriaDTO categoria)
        {
            string script =
                @"INSERT INTO tb_categoria (nm_categoria) VALUES (@nm_categoria)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", categoria.nm_categoria));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(CategoriaDTO categoria)
        {
            string script =
            @"UPDATE tb_categoria
                 SET 
                  nm_categoria = @nm_categoria
                  WHERE id_categoria = @id_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_categoria", categoria.id_categoria));
            parms.Add(new MySqlParameter("nm_categoria", categoria.nm_categoria));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_categoria WHERE id_categoria = @id_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_categoria", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<CategoriaDTO> Listar()
        {
            string script =
                @"SELECT * FROM tb_categoria";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CategoriaDTO> categorias  = new List<CategoriaDTO>();
            while (reader.Read())
            {

                CategoriaDTO novacategoria = new CategoriaDTO();
                novacategoria.id_categoria = reader.GetInt32("id_categoria");
                novacategoria.nm_categoria = reader.GetString("nm_categoria");

                categorias.Add(novacategoria);

            }
            reader.Close();
            return categorias;
        }

        public List<CategoriaDTO> Consultar(string categoria)
        {

            string script =
                @"SELECT * FROM tb_categoria
                  WHERE nm_categoria like @nm_categoria";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + categoria + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CategoriaDTO> categorias = new List<CategoriaDTO>();
            while (reader.Read())
            {

                CategoriaDTO novacategoria = new CategoriaDTO();
                novacategoria.id_categoria = reader.GetInt32("id_categoria");
                novacategoria.nm_categoria = reader.GetString("nm_categoria");

                categorias.Add(novacategoria);

            }
            reader.Close();
            return categorias;
        }

        public List<LivroViewDTO> Lista()
        {
            string script =
                @"SELECT * FROM tb_categoria";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<LivroViewDTO> categorias = new List<LivroViewDTO>();
            while (reader.Read())
            {

                LivroViewDTO novacategoria = new LivroViewDTO();
                novacategoria.id_categoria = reader.GetInt32("id_categoria");
                novacategoria.nm_categoria = reader.GetString("nm_categoria");

                categorias.Add(novacategoria);

            }
            reader.Close();
            return categorias;
        }


    }
}
