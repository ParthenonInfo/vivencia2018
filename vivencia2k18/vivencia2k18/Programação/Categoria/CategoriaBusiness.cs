﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Categoria
{
   public class CategoriaBusiness
    {
        CategoriaDatabase db = new CategoriaDatabase();

        public int Salvar(CategoriaDTO categoria)
        {
            return db.Salvar(categoria);
        }
        public int Alterar(CategoriaDTO categoria)
        {
            return db.Salvar(categoria);
        }
        public List<CategoriaDTO> Consultar(string categoria)
        {
            return db.Consultar(categoria);
        }

        public List<CategoriaDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}
