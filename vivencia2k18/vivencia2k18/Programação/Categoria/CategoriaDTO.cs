﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Categoria
{
    public class CategoriaDTO
    {
        public int id_categoria { get; set; }
        public string nm_categoria { get; set; }
    }
}
