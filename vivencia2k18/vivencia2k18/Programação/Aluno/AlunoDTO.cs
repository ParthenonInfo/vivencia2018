﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Aluno
{
   public class AlunoDTO
    {
        public int id_aluno { get; set; }
        public string nm_aluno { get; set; }
        public string nm_turma { get; set; }
        public string ds_email { get; set; }
        public string ds_rb { get; set; }
    }
}
