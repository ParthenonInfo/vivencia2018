﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Aluno
{
    public class AlunoBusiness
    {
        AlunoDatabase db = new AlunoDatabase();

        public int Salvar(AlunoDTO aluno)
        {
            return db.Salvar(aluno);
        }
        public int Alterar(AlunoDTO aluno)
        {
            return db.Salvar(aluno);
        }
        public List<AlunoDTO> Consultar(string aluno)
        {
            return db.Consultar(aluno);
        }

        public List<AlunoDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }


    }
}
