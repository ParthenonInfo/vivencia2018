﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Aluno
{
  public  class AlunoDatabase
    {
        public int Salvar(AlunoDTO aluno)
        {
            string script =
                @"INSERT INTO tb_aluno
                (
                id_aluno,
                nm_aluno,   
                nm_turma,
                ds_email,
                ds_rb
                )
                VALUES
                (
                @id_aluno,
                @nm_aluno,   
                @nm_turma,
                @ds_email,
                @ds_rb
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_aluno", aluno.id_aluno));
            parms.Add(new MySqlParameter("nm_aluno", aluno.nm_aluno));
            parms.Add(new MySqlParameter("nm_turma", aluno.nm_turma));
            parms.Add(new MySqlParameter("ds_email", aluno.ds_email));
            parms.Add(new MySqlParameter("ds_rb", aluno.ds_rb));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(AlunoDTO aluno)
        {
            string script =
            @"UPDATE tb_aluno
                 SET
                  nm_aluno = @nm_aluno,
                  nm_turma = @nm_turma,
                  ds_email= @ds_email,
                  ds_rb = @ds_rb
                  WHERE id_aluno = @id_aluno";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_aluno", aluno.id_aluno));
            parms.Add(new MySqlParameter("nm_aluno", aluno.nm_aluno));
            parms.Add(new MySqlParameter("nm_turma", aluno.nm_turma));
            parms.Add(new MySqlParameter("ds_email", aluno.ds_email));
            parms.Add(new MySqlParameter("ds_rb", aluno.ds_rb));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_aluno WHERE id_aluno = @id_aluno";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_aluno", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<AlunoDTO> Listar()
        {
            string script =
                @"SELECT * FROM tb_aluno";
      

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script,null);

            List<AlunoDTO> alunos = new List<AlunoDTO>();
            while (reader.Read())
            {

                AlunoDTO novoaluno = new AlunoDTO();
                novoaluno.id_aluno = reader.GetInt32("id_aluno");
                novoaluno.nm_aluno = reader.GetString("nm_aluno");
                alunos.Add(novoaluno);

            }
            reader.Close();
            return alunos;
        }

        public List<AlunoDTO> Consultar(string aluno)
        {
            string script =
                @"SELECT * FROM tb_aluno
                  WHERE nm_aluno like @nm_aluno";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_aluno", "%" + aluno + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<AlunoDTO> alunos = new List<AlunoDTO>();
            while (reader.Read())
            {

                AlunoDTO novoaluno = new AlunoDTO();
                novoaluno.id_aluno = reader.GetInt32("id_aluno");
                novoaluno.nm_aluno = reader.GetString("nm_aluno");
                novoaluno.nm_turma = reader.GetString("nm_turma");
                novoaluno.ds_email = reader.GetString("ds_email");
                novoaluno.ds_rb = reader.GetString("ds_rb");

                alunos.Add(novoaluno);

            }
            reader.Close();
            return alunos;
        }
    }
}
