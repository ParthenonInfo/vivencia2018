﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Usuario
{
    class UsuarioBusiness
    {
        public int Salvar(UsuarioDTO dto)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Salvar(dto);
        }
        public List<UsuarioDTO> Consultar(string usuario)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Consultar(usuario);
        }
        public void Alterar(UsuarioDTO dto)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            db.Remover(id);
        }
        public List<UsuarioDTO> Listar()
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Listar();
        }

        public UsuarioDTO Logar(string user, string senha)
        {
         
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Logar(user, senha);
 
        }
    }
}
