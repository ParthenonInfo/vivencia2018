﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Usuario
{
    class UsuarioDatabase
    {
        public UsuarioDTO Logar(string user, string senha)
        {
            string script = @"SELECT * FROM tb_usuario WHERE nm_login = @nm_login AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_login", user));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            UsuarioDTO usuario = null;

            if (reader.Read())
            {
                usuario = new UsuarioDTO();
              //  user.ID = reader.GetInt32("id_usuario");
                usuario.nm_login = reader.GetString("nm_login");
                usuario.ds_senha = reader.GetString("ds_senha");
                
            }

            reader.Close();

            return usuario;
        }

        public int Salvar(UsuarioDTO dto)
        {
            string script = @" INSERT INTO tb_usuario 
            (nm_login, ds_senha) 
            VALUES (@nm_login, @ds_senha)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_login", dto.nm_login));
            parms.Add(new MySqlParameter("ds_senha", dto.nm_login));
           


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(UsuarioDTO dto)
        {
            string script = @"UPDATE tb_usuario 
                                 SET nm_login = @nm_login,
                                     ds_senha = @ds_senha   

                                     WHERE id_usuario = @id_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
           // parms.Add(new MySqlParameter("id_usuario", dto.id_usuario));
            parms.Add(new MySqlParameter("nm_login", dto.nm_login));
            parms.Add(new MySqlParameter("ds_senha", dto.ds_senha));
           
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {

            string script = @"DELETE FROM tb_usuario WHERE id_usuario = @id_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_usuario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<UsuarioDTO> Consultar(string usuario)
        {
            string script = @"SELECT * FROM tb_usuario WHERE nm_login like @nm_login";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<UsuarioDTO> lista = new List<UsuarioDTO>();
            while (reader.Read())
            {
                UsuarioDTO dto = new UsuarioDTO();
               // dto.id_usuario = reader.GetInt32("id_usuario");
                dto.nm_login = reader.GetString("nm_login");
                dto.ds_senha = reader.GetString("ds_senha");
                

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<UsuarioDTO> Listar()
        {
            string script = "select * from tb_usuario";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<UsuarioDTO> listar = new List<UsuarioDTO>();

            while (reader.Read())
            {
                UsuarioDTO dto = new UsuarioDTO();
               // dto.id_usuario = reader.GetInt32("id_usuario");
                dto.nm_login = reader.GetString("nm_login");
                dto.ds_senha = reader.GetString("ds_senha");
                
                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }

    }
}
