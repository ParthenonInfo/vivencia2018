﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Autor
{
    public class AutorDTO
    {
        public int id_autor { get; set; }
        public string nm_autor { get; set; }

    }
}
