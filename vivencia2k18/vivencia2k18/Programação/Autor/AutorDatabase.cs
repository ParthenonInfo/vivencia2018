﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Autor
{
   public class AutorDatabase
    {
        public int Salvar(AutorDTO dto)
        {
            string script = @" INSERT INTO tb_autor 
            (nm_autor) 
            VALUES (@nm_autor)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_autor", dto.nm_autor));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(AutorDTO dto)
        {
            string script = @"UPDATE tb_autor 
                                 SET nm_autor = @nm_autor 

                                     WHERE id_autor = @id_autor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_autor", dto.nm_autor));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {

            string script = @"DELETE FROM tb_autor WHERE id_autor = @id_autor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_autor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<AutorDTO> Consultar(string autor)
        {
            string script = @"SELECT * FROM tb_autor WHERE nm_autor like @nm_autor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_autor", autor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AutorDTO> lista = new List<AutorDTO>();
            while (reader.Read())
            {
                AutorDTO dto = new AutorDTO();
                dto.nm_autor = reader.GetString("nm_autor");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<AutorDTO> Listar()
        {
            string script = "select * from tb_autor";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AutorDTO> listar = new List<AutorDTO>();

            while (reader.Read())
            {
                AutorDTO dto = new AutorDTO();
                dto.nm_autor = reader.GetString("nm_autor");

                listar.Add(dto);

            }
            reader.Close();
            return listar;

        }
    }
}
