﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Autor
{
   public class AutorBusiness
    {
        public int Salvar(AutorDTO dto)
        {
            AutorDatabase db = new AutorDatabase();
            return db.Salvar(dto);
        }
        public List<AutorDTO> Consultar(string autor)
        {
            AutorDatabase db = new AutorDatabase();
            return db.Consultar(autor);
        }
        public void Alterar(AutorDTO dto)
        {
            AutorDatabase db = new AutorDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            AutorDatabase db = new AutorDatabase();
            db.Remover(id);
        }
        public List<AutorDTO> Listar()
        {
            AutorDatabase db = new AutorDatabase();
            return db.Listar();
        }
    }
}
