﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Emprestimo
{
  public  class EmprestimoDatabase
    {
        public int Salvar(EmprestimoDTO emprestimo)
        {
            string script =
                @"INSERT INTO tb_emprestimo
                (
                id_emprestimo,
                id_aluno,   
                id_livro,
                dt_emprestimo,
                dt_vencimento, 
                ds_foidevolvido
                ds_observacao,
                
                )
                VALUES
                (
                @id_emprestimo,
                @id_aluno,   
                @id_livro,
                @dt_emprestimo,
                @dt_vencimento,
               "",
               ""
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_emprestimo", emprestimo.id_emprestimo));
            parms.Add(new MySqlParameter("id_aluno", emprestimo.id_aluno));
            parms.Add(new MySqlParameter("dt_emprestimo", emprestimo.dt_emprestimo));
            parms.Add(new MySqlParameter("dt_vencimento", emprestimo.dt_vencimento));
            parms.Add(new MySqlParameter("ds_foidevolvido", emprestimo.ds_foidevolvido));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(EmprestimoDTO emprestimo)
        {
            string script =
            @"UPDATE tb_emprestimo
                 SET 
                  id_aluno = @id_aluno,
                  id_livro = @id_livro,
                  dt_emprestimo = @dt_emprestimo,
                  dt_vencimento = @dt_vencimento,
                 
                  WHERE id_emprestimo = @id_emprestimo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_emprestimo", emprestimo.id_emprestimo));
            parms.Add(new MySqlParameter("id_aluno", emprestimo.id_aluno));
            parms.Add(new MySqlParameter("dt_emprestimo", emprestimo.dt_emprestimo));
            parms.Add(new MySqlParameter("dt_vencimento", emprestimo.dt_vencimento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_emprestimo WHERE id_emprestimo = @id_emprestimo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_emprestimo", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<EmprestimoDTO> Listar()
        {
            string script =
                @"SELECT * FROM tb_emprestimo";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EmprestimoDTO> emprestimos = new List<EmprestimoDTO>();
            while (reader.Read())
            {

                EmprestimoDTO novoemprestimo = new EmprestimoDTO();
                novoemprestimo.id_emprestimo = reader.GetInt32("id_emprestimo");
                novoemprestimo.id_aluno.id_aluno = reader.GetInt32("id_aluno");
                novoemprestimo.dt_emprestimo = reader.GetDateTime("dt_emprestimo");
                novoemprestimo.dt_vencimento = reader.GetDateTime("dt_vencimento");
        
                emprestimos.Add(novoemprestimo);
            }
            reader.Close();
            return emprestimos;
        }

        public List<EmprestimoView> ConsultarView(string aluno)
        {

            string script =
                @"SELECT * FROM emprestimo
                  WHERE nm_aluno like @nm_aluno";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_aluno", "%" + aluno + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EmprestimoView> emprestimos = new List<EmprestimoView>();
            while (reader.Read())
            {

                EmprestimoView novoemprestimo = new EmprestimoView();
                novoemprestimo.nm_aluno = reader.GetString("nm_aluno");
                novoemprestimo.nm_livro = reader.GetString("nm_livro");
                novoemprestimo.dt_emprestimo = reader.GetDateTime("dt_emprestimo");
                novoemprestimo.dt_vencimento = reader.GetDateTime("dt_vencimento");

                emprestimos.Add(novoemprestimo);
            }
            reader.Close();
            return emprestimos;
        }
        public List<EmprestimoDTO> Consultar(string emprestimo)
        {

            string script =
                @"SELECT * FROM tb_emprestimo
                  WHERE dt_vencimento like @dt_vencimento";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + emprestimo + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EmprestimoDTO> emprestimos = new List<EmprestimoDTO>();
            while (reader.Read())
            {

                EmprestimoDTO novoemprestimo = new EmprestimoDTO();
                novoemprestimo.id_emprestimo = reader.GetInt32("id_emprestimo");
                novoemprestimo.id_aluno.id_aluno = reader.GetInt32("id_aluno");
                novoemprestimo.dt_emprestimo = reader.GetDateTime("dt_emprestimo");
                novoemprestimo.dt_vencimento = reader.GetDateTime("dt_vencimento");

                emprestimos.Add(novoemprestimo);
            }
            reader.Close();
            return emprestimos;
        }

        public int salvar(EmprestimoDTO dto)
        {
            string script = @"INSERT INTO `db_biblioteca`.`tb_emprestimo` (id_aluno, dt_emprestimo, dt_vencimento) VALUES (@id_aluno,@dt_emprestimo,@dt_vencimento)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_aluno", dto.id_aluno.id_aluno));
            parms.Add(new MySqlParameter("dt_emprestimo", dto.dt_emprestimo));
            parms.Add(new MySqlParameter("dt_vencimento", dto.dt_vencimento));



            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }


        public void Updatemais(int Id)
        {
            string script = @"UPDATE `db_biblioteca`.`tb_estoque` 
                            SET `ds_quantidade_emprestada` =ds_quantidade_emprestada+1 
                            WHERE id_livro=@id_livro";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", Id));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
