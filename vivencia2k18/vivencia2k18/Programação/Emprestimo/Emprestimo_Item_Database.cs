﻿
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Emprestimo
{
   public class Emprestimo_Item_Database
    {
        public void Salvar(Emprestimo_Item_DTO dto)
        {
            string script = @"INSERT INTO `db_biblioteca`.`tb_emprestimo_item` (id_livro,id_emprestimo) VALUES (@id_livro,@id_emprestimo)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", dto.livro.id_livro));
            parms.Add(new MySqlParameter("id_emprestimo", dto.teste.id_emprestimo));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
    

    }
}
