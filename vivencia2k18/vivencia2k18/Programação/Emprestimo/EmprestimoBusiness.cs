﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Emprestimo
{
    public class EmprestimoBusiness
    {
        EmprestimoDatabase db = new EmprestimoDatabase();

        public int Salvar(EmprestimoDTO emprestimo)
        {
            return db.Salvar(emprestimo);
        }
        public int Alterar(EmprestimoDTO emprestimo)
        {
            return db.Salvar(emprestimo);
        }
        public List<EmprestimoDTO> Consultar(string emprestimo)
        {
            return db.Consultar(emprestimo);
        }

        public List<EmprestimoDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }


        public void Salvar(EmprestimoDTO salvar, List<Emprestimo_Item_DTO> testeitem)
        {
            EmprestimoDatabase database = new EmprestimoDatabase();
            int pk = database.salvar(salvar);

            Emprestimo_Item_Business itens = new Emprestimo_Item_Business();

            foreach (Emprestimo_Item_DTO item in testeitem)
            {
                item.teste = new EmprestimoDTO();
                item.teste.id_emprestimo = pk;

                itens.Salvar(item);
            }
        }

        public List<EmprestimoView> ConsultarView(string aluno)
        {
            EmprestimoDatabase database = new EmprestimoDatabase();
            return database.ConsultarView(aluno);
        }
    }

}
