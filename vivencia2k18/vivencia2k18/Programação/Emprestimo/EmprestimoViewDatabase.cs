﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Emprestimo
{
  public   class EmprestimoViewDatabase
    {

        public void Update(EmprestimoView dto)
        {
            string script = @"UPDATE `db_biblioteca`.`tb_estoque` 
                            SET `ds_quantidade_emprestada` =ds_quantidade_emprestada-1 
                            WHERE id_livro=@id_livro";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_livro", dto.id_livro));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<EmprestimoView> Consultar(string livro)
        {
            string script = @"SELECT * FROM emprestimo WHERE nm_livro like @nm_livro AND ds_foidevolvido = 1";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro", livro + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EmprestimoView> lista = new List<EmprestimoView>();
            while (reader.Read())
            {
                EmprestimoView dto = new EmprestimoView();

                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_emprestimo = reader.GetDateTime("dt_emprestimo");
                dto.dt_vencimento = reader.GetDateTime("dt_vencimento");
                dto.nm_aluno = reader.GetString("nm_aluno");
                dto.id_livro = reader.GetInt32("id_livro");
                dto.id_emprestimo = reader.GetInt32("id_emprestimo");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Alteraçao(EmprestimoView dto)
        {
            string script = @"UPDATE tb_emprestimo
                                 SET ds_foidevolvido = 0,
                                 ds_observacao=@ds_observacao
                                     WHERE id_emprestimo =@id_emprestimo";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_emprestimo", dto.id_emprestimo));
            parms.Add(new MySqlParameter("ds_observacao", dto.ds_observacao));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public void Alteraçaodata(EmprestimoView dto)
        {
            string script = @"UPDATE `db_biblioteca`.`tb_emprestimo` 
                            SET `dt_vencimento`=@dt_vencimento 
                            WHERE id_emprestimo=@id_emprestimo";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_emprestimo", dto.id_emprestimo));
            parms.Add(new MySqlParameter("dt_vencimento", dto.dt_vencimento));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<EmprestimoView> ConsultarView(string aluno)
        {

            string script =
                @"SELECT * FROM emprestimo
              WHERE nm_aluno like @nm_aluno";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_aluno", "%" + aluno + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EmprestimoView> emprestimos = new List<EmprestimoView>();
            while (reader.Read())
            {

                EmprestimoView novoemprestimo = new EmprestimoView();
                novoemprestimo.nm_aluno = reader.GetString("nm_aluno");
                novoemprestimo.nm_livro = reader.GetString("nm_livro");
                novoemprestimo.dt_emprestimo = reader.GetDateTime("dt_emprestimo");
                novoemprestimo.dt_vencimento = reader.GetDateTime("dt_vencimento");

                emprestimos.Add(novoemprestimo);
            }
            reader.Close();
            return emprestimos;
        }


        public List<AtrasoView> ConsultarAtrasos()
        {

            string script = @"SELECT * FROM AtrasoView WHERE dt_vencimento BETWEEN ('dt_vencimento') AND now() AND ds_foidevolvido = 1; ";


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<AtrasoView> lista = new List<AtrasoView>();
            while (reader.Read())
            {
                AtrasoView dto = new AtrasoView();

                dto.nm_livro = reader.GetString("nm_livro");
                dto.dt_emprestimo = reader.GetDateTime("dt_emprestimo");
                dto.dt_vencimento = reader.GetDateTime("dt_vencimento");
                dto.nm_categoria = reader.GetString("nm_categoria");
                dto.nm_aluno = reader.GetString("nm_aluno");
                dto.id_livro = reader.GetInt32("id_livro");
                dto.id_emprestimo = reader.GetInt32("id_emprestimo");
                dto.ds_foidevolvido = reader.GetBoolean("ds_foidevolvido");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
