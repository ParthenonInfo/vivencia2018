﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Emprestimo
{
   public class EmprestimoViewBusiness
    {
        public List<EmprestimoView> Consultar(string livro)
        {
            EmprestimoViewDatabase db = new EmprestimoViewDatabase();
            return db.Consultar(livro);
        }
        public List<AtrasoView> ConsultarAtrasos()
        {
            EmprestimoViewDatabase db = new EmprestimoViewDatabase();
            List<AtrasoView> lista = db.ConsultarAtrasos();
            return lista;
        }


    }
}
