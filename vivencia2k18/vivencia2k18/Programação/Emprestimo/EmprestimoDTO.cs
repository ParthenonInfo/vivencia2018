﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.Programação.Aluno;

namespace vivencia2k18.Programação.Emprestimo
{
   public  class EmprestimoDTO
    {
        public int id_emprestimo { get; set; }
        public AlunoDTO id_aluno { get; set; }
        public DateTime dt_emprestimo { get; set; }
        public DateTime dt_vencimento { get; set; }

        public string ds_observacao { get; set; }

        public bool ds_foidevolvido { get; set; }


    }
}
