﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vivencia2k18.DB;

namespace vivencia2k18.Programação.Emprestimo
{
   public class Emprestimo_devolucao_view_Database
    {
        public List<Emprestimo_devolucao_view> Listar()
        {
            string script =
                @"select * from devolucao where ds_foidevolvido=0";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Emprestimo_devolucao_view> emprestimos = new List<Emprestimo_devolucao_view>();
            while (reader.Read())
            {

                Emprestimo_devolucao_view devoluçao = new Emprestimo_devolucao_view();
                devoluçao.nm_aluno = reader.GetString("nm_aluno");
                devoluçao.nm_livro = reader.GetString("nm_livro");
                devoluçao.ds_observacao = reader.GetString("ds_observacao");
    
                emprestimos.Add(devoluçao);
            }
            reader.Close();
            return emprestimos;
        }






    }
}
