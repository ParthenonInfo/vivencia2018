﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vivencia2k18.Programação.Emprestimo
{
    public class AtrasoView
    {
        public int id_emprestimo { get; set; }
        public int id_livro { get; set; }
        public string nm_aluno { get; set; }
        public string nm_livro { get; set; }
        public string nm_categoria { get; set; }
        public DateTime dt_vencimento { get; set; }
        public DateTime dt_emprestimo { get; set; }
        public bool ds_foidevolvido { get; set; }

    }
}
